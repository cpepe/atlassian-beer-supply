package com.example.beersupply.rest;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.example.beersupply.brewerydb.BreweryDBClient;
import com.example.beersupply.components.BeerRatingManager;
import com.example.beersupply.components.BeerSupplyConfigurationManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class BeerSupplyResourceTest {

    private BeerSupplyResource resource;

    @Mock
    private BeerRatingManager beerRatingManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private BreweryDBClient breweryDBClient;
    @Mock
    private BeerSupplyConfigurationManager beerSupplyConfigurationManager;
    @Mock
    private WebResourceUrlProvider webResourceUrlProvider;


    @Before
    public void setup() {
        resource = new BeerSupplyResource(beerRatingManager, authenticationContext, breweryDBClient,
                beerSupplyConfigurationManager, webResourceUrlProvider);
    }

    @Test
    public void setRating() {
        when(beerRatingManager.updateOrAddRating(anyString(), any(User.class), anyDouble()))
                .thenReturn(5.0d);

        Response r = resource.setRating("TST-1", 4.0d);

        assertEquals("5.0", r.getEntity().toString());
    }

    @Test
    public void getUserRating() {
        when(beerRatingManager.getUserRating(anyString(), any(User.class)))
                .thenReturn(3.2d);

        Response r = resource.getUserRating("TST-1");

        assertEquals("3.2", r.getEntity().toString());
    }
}
