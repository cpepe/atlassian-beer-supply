package com.example.beersupply.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import com.example.beersupply.brewerydb.BreweryDBClient;
import com.example.beersupply.brewerydb.representations.BeerDescriptor;
import com.example.beersupply.components.BeerRatingManager;
import com.example.beersupply.components.BeerSupplyConfigurationManager;

import org.apache.commons.lang.StringUtils;

/**
 * @since version
 */
@Path("/beer")
public class BeerSupplyResource
{
    private final BeerRatingManager ratingManager;
    private final JiraAuthenticationContext authContext;
    private final BreweryDBClient breweryDBClient;
    private final BeerSupplyConfigurationManager configManager;
    private final WebResourceUrlProvider webResourceUrlProvider;
    
    public BeerSupplyResource(BeerRatingManager ratingManager, JiraAuthenticationContext authContext, BreweryDBClient breweryDBClient, BeerSupplyConfigurationManager configManager, WebResourceUrlProvider webResourceUrlProvider)
    {
        this.ratingManager = ratingManager;
        this.authContext = authContext;
        this.breweryDBClient = breweryDBClient;
        this.configManager = configManager;
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    /**
     * Applies the specified rating to the specified issue on the currently logged-in
     * user's behalf.
     * @param issueKey the beer issue to rate
     * @param rating the rating to give
     * @return a JSON or XML representation of the new average rating
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Path("/{issueKey}/rate")
    public Response setRating(@PathParam("issueKey") String issueKey, Double rating)
    {
        // The BeerRatingManager offers the method updateOrAddRating(), which takes
        // an issue key, a User object, and a rating. We have the issue key and rating
        // already from the parameters, so we need to get the currently logged-in user.
        // That's accessible through the JIRAAuthenticationContext.getLoggedInUser()
        // method.
        //
        // Once we have all the pieces, we can call updateOrAddRating() and use its
        // return value as the entity of a new Response object.

        User user = authContext.getLoggedInUser();
        Double average = ratingManager.updateOrAddRating(issueKey,user,rating);

        return Response.ok(average).build();
    }

    /**
     * Returns the currently logged-in user's rating for the specified issue.
     * @param issueKey the beer issue to get the rating for
     * @return a JSON or XML representation of the user's rating
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Path("/{issueKey}/rating/user")
    public Response getUserRating(@PathParam("issueKey") String issueKey)
    {
        // The BeerRatingManager offers the method getUserRating(), which takes
        // an issue key and a User object. We have the issue key already from
        // the parameters, so we need to get the currently logged-in user.
        // That's accessible through the JIRAAuthenticationContext.getLoggedInUser()
        // method.
        //
        // Once we have those pieces, we can call getUserRating() and use its return
        // value as the entity of a new Response object.
        User user = authContext.getLoggedInUser();
        Double rating = ratingManager.getUserRating(issueKey,user);

        return Response.ok(rating).build();
    }
    
    @GET
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Path("/search/{searchTerm}")
    public Response searchForBeers(@PathParam("searchTerm") String searchTerm)
    {
        List<BeerDescriptor> beers = breweryDBClient.search(searchTerm, configManager.getBreweryDBApiKey());
        
        //make sure we have images for labels
        for(BeerDescriptor beer : beers)
        {
            if(StringUtils.isBlank(beer.getIcon()))
            {
                String beerIcon = webResourceUrlProvider.getStaticPluginResourceUrl("com.example.beersupply.atlassian-beer-supply:beer-supply-resources", "default-beer-icon.png", UrlMode.ABSOLUTE);
                beer.setIcon(beerIcon);
            }

            if(StringUtils.isBlank(beer.getLabel()))
            {
                String beerLabel = webResourceUrlProvider.getStaticPluginResourceUrl("com.example.beersupply.atlassian-beer-supply:beer-supply-resources", "default-beer-label.png", UrlMode.ABSOLUTE);
                beer.setLabel(beerLabel);
            }
        }

        return Response.ok(beers).build();
    }
    
}
