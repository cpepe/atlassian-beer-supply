 AJS.namespace("beersearch");
(function ()
{
    // Declare a local copy of beerview to instantiate on the window object.
    // access with the global "beerview" variable.
    var localBeerSearch = function ()
    {
        var searchDialog;
        var searchTermsField;
        var searchResultsContainer;
        this.init = function(serachContainerId,searchTermsId,searchButtonId,searchResultsId)
        {
            //clear out old values in-case we're doing an "add another"
            jQuery("#description").val("");
            jQuery(".beer-style-select").val("");
            jQuery(".beer-label-field").val("");
            jQuery(".brewery-db-search-field").val("");
            
            searchTermsField = jQuery(searchTermsId);
            searchResultsContainer = jQuery(searchResultsId);
            
            jQuery(searchButtonId).bind('click',function()
            {
                beersearch.doSearch(searchTermsField.val());
            });
            
            dialog = new AJS.Dialog({width:820, height:526, id:"beer-search-dialog", closeOnOutsideClick: true});
            dialog.addHeader(AJS.I18n.getText("beer-supply.brewerydb-search-field.name"));
            dialog.addPanel("Search", serachContainerId);
            dialog.addButton("Cancel", function (dialog) {
                dialog.hide();
            });
        };
        
        this.showSearchDialog = function(searchTerm)
        {
            searchTermsField.val(searchTerm);
            dialog.show();
            this.doSearch(searchTerm);
        };
        
        this.doSearch = function(searchTerm)
        {
            searchResultsContainer.empty();
            AJS.$(AJS.$.ajax(AJS.contextPath() + "/rest/beersupply/1.0/beer/search/" + searchTerm,
            {
                'type':'GET',
                'processData':false,
                'dataType':'json',
                'contentType':'application/json',
                'success':function (data)
                {
                    var nameHeader = AJS.I18n.getText("beer-supply.beertable.name.header");
                    var styleHeader = AJS.I18n.getText("beer-supply.beertable.style.header");
                    var breweryHeader = AJS.I18n.getText("beer-supply.beertable.brewery.header");
                    searchResultsContainer.append("<table id='beer-table' class='aui'><thead><th id='beer-icon'></th><th id='beer-name'>" + nameHeader + "</th><th id='beer-brewery'>" + breweryHeader + "</th><th id='beer-style'>" + styleHeader + "</th></thead>")
                    var beerTable = jQuery('#beer-table');
                    var template = AJS.template("");
                    jQuery.each(data,function(index)
                    {
                        jQuery(AJS.template.load('beer-row-template').fill(data[index]).toString()).appendTo(beerTable).bind('click',function(){
                            beersearch.selectBeer(data[index]);
                        });
                    });

                    beerTable.append("</table>");
                }
            })).throbber({
                target:searchResultsContainer,
                isLatentThreshold: 150,
                minThrobberDisplay: 400,
                loadingClass: "brewery-db-search-throbber"
            });   
        };
        
        this.selectBeer = function(data)
        {
            jQuery("#summary").val(data.name);
            jQuery("#description").val(data.description);
            //TODO: remove hard coding and support a custom field
            jQuery("#customfield_10000").val(data.style);
            //jQuery(".beer-style-select").val(data.style);
            //jQuery(".beer-label-field").val(data.label);
            jQuery(".brewery-db-search-field").val("");
            dialog.hide();
        }
    };

    // Globally register beerview. We are using 'new' so that the 'this' references in beerview
    // correctly point to the beerview object, not the window.
    window.beersearch = new localBeerSearch();

})();