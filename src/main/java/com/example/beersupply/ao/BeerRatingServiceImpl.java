package com.example.beersupply.ao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.activeobjects.external.ActiveObjects;

import com.google.common.collect.ImmutableList;

import com.google.common.collect.ImmutableSet;
import net.java.ao.DBParam;
import net.java.ao.EntityStreamCallback;
import net.java.ao.Query;

/**
 * @since version
 */
public class BeerRatingServiceImpl implements BeerRatingService
{
    private final ActiveObjects ao;    

    public BeerRatingServiceImpl(ActiveObjects ao)
    {
        this.ao = ao;
    }

    /**
     * Records a rating for the specified beer issue by the specified user.
     * @param issueKey the beer to rate
     * @param userName the user making the rating
     * @param rating the rating itself
     * @return the {@code BeerRating} entity object for the created rating
     */
    @Override
    public BeerRating add(String issueKey, String userName, Double rating)
    {
        // To add a new BeerRating, we create a new instance of the BeerRating
        // entity using the ActiveObjects object and its create() method.
        // The first parameter is the entity object, in this case BeerRating,
        // and the remaining parameters are instances of DBParam.
        //
        // DBParams are created according to the form:
        //
        // DBParam param = new DBParam(<column_name>, <column_value>
        //
        // where <column_name> is a String and <column_value> is a String
        // or primitive value.
        //
        // In this class, the parent interface BeerRatingService provides the
        // predefined constants DBPARAM_ISSUE_KEY, DBPARAM_USERNAME, and
        // DBPARAM_RATING for the column names. To create a new BeerRating,
        // use ao.create() with the BeerRating entity class and the three
        // DBParam objects for issue key, user name, and rating.

        return ao.create(BeerRating.class,
                new DBParam(DBPARAM_ISSUE_KEY, issueKey),
                new DBParam(DBPARAM_USERNAME, userName),
                new DBParam(DBPARAM_RATING, rating));
    }

    /**
     * Deletes the specified user's rating from the specified beer issue.
     * @param issueKey the beer to unrate
     * @param username the user removing their rating
     */
    @Override
    public void delete(String issueKey, String username)
    {
        // Deletion in ActiveObjects requires us to first obtain the entity object
        // corresponding to the row we want to delete. While we could find it ourselves
        // using the ao.find() method, in this case we can reuse the getRatingForIssueByUser()
        // method already in this class. That method returns an entity object for a beer
        // issue and a username; once we have it, we can pass it to the ao.delete() method.

        BeerRating goner = getRatingForIssueByUser(issueKey, username);
        ao.delete(goner);
    }

    /**
     * Returns an {@code Iterable<String>} of all usernames who have made
     * a rating on any beer.
     * @return an {@code Iterable<String>} of usernames
     */
    @Override
    public Iterable<String> getVotingUsers() {
        // The primary method for querying ActiveObjects is the ao.find() method,
        // which takes an entity class object and (optionally) a Query object
        // specifying column filters.
//        final Set<String> usernames = new HashSet<String>();
//
//        ao.stream(BeerRating.class, Query.select("*"), new EntityStreamCallback<BeerRating, Integer>() {
//            @Override
//            public void onRowRead(BeerRating rating) {
//                usernames.add(rating.getUsername());
//            }
//        });
//
//        return ImmutableSet.copyOf(usernames);
        BeerRating[] ratings = ao.find(BeerRating.class);
        final Set<String> usernames = new HashSet<String>();
        for (BeerRating rating: ratings) {
            usernames.add(rating.getUsername());
        }
        return usernames;
    }

    @Override
    public List<BeerRating> getRatingsForIssue(String issueKey)
    {
        final List<BeerRating> ratings = new ArrayList<BeerRating>();

        ao.stream(BeerRating.class, Query.select("*").where("issue_key = ?", issueKey),new EntityStreamCallback<BeerRating, Integer>() {
            @Override
            public void onRowRead(BeerRating rating)
            {
                ratings.add(rating);
            }
        });

        return ImmutableList.copyOf(ratings);
    }

    @Override
    public BeerRating getRatingForIssueByUser(String issueKey, String username)
    {
        BeerRating[] ratings = ao.find(BeerRating.class, Query.select().where("issue_key = ? AND username = ?",issueKey,username).limit(1));

        if(null != ratings && ratings.length > 0)
        {
            return ratings[0];
        }

        return null;
    }

    @Override
    public BeerRating getById(Integer id)
    {
        return ao.get(BeerRating.class,id);
    }

    @Override
    public int getCount()
    {
        return ao.count(BeerRating.class);
    }

    @Override
    public int getCountByIssue(String issueKey)
    {
        return ao.count(BeerRating.class,Query.select().where("issue_key = ?", issueKey));
    }

    @Override
    public Double getSumByIssue(String issueKey)
    {
        List<BeerRating> ratings = getRatingsForIssue(issueKey);
        Double sum = 0.0;

        for(BeerRating rating : ratings)
        {
            sum += rating.getRating();
        }

        return sum;
    }
}
