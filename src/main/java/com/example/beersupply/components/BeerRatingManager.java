package com.example.beersupply.components;

import java.util.HashMap;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.*;
import com.atlassian.jira.issue.fields.CustomField;
import com.example.beersupply.ao.BeerRating;
import com.example.beersupply.ao.BeerRatingService;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

/**
 * @since version
 */
public class BeerRatingManager
{
    private final BeerRatingService ratingService;
    private final IssueManager issueManager;
    private final CustomFieldManager customFieldManager;

    public BeerRatingManager(BeerRatingService ratingService, IssueService issueService, IssueManager issueManager, CustomFieldManager customFieldManager)
    {
        this.ratingService = ratingService;
        this.issueManager = issueManager;
        this.customFieldManager = customFieldManager;
    }

    public Double updateOrAddRating(String issueKey, User user, Double rating)
    {
        Issue issue = issueManager.getIssueObject(issueKey);
        if(null == issue)
        {
            return 0.0;
        }

        BeerRating beerRating = ratingService.getRatingForIssueByUser(issueKey,user.getName());

        if(null != beerRating)
        {
            beerRating.setRating(rating);
            beerRating.save();
        }
        else
        {
            ratingService.add(issueKey,user.getName(),rating);
        }
        return rating;
    }

    public Double getUserRating(String issueKey, User user)
    {
        Double rating = 0.0;
        BeerRating beerRating;
        try
        {
            beerRating = ratingService.getRatingForIssueByUser(issueKey,user.getName());
        }
        catch(Exception e)
        {
            beerRating = null;
        }
        if(null != beerRating)
        {
            rating = beerRating.getRating();
        }
        return rating;
    }
}
