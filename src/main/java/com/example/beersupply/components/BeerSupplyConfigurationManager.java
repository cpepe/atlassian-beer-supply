package com.example.beersupply.components;

import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import com.example.beersupply.brewerydb.BreweryDBClient;
import com.example.beersupply.brewerydb.representations.Style;
import com.example.beersupply.exception.BeerSupplySetupException;

import org.apache.commons.lang.StringUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since version
 */
public class BeerSupplyConfigurationManager
{
    private static final String SETTINGS_PROJECT_KEY = "com.example.beersupply:projectKey";
    private static final String SETTINGS_BREWERYDB_KEY = "com.example.beersupply:breweryDBKey";
    
    /**
     * The ActiveObjects component will be injected
     */
    /*
    private final PluginSettingsFactory pluginSettingsFactory;
    private final BeerSupplyProjectCreator projectCreator;
    private final BeerSupplyWorkflowCreator workflowCreator;
    private final BeerSupplyScreenCreator screenCreator;
    private final BreweryDBClient breweryDBClient;

    public BeerSupplyConfigurationManager(PluginSettingsFactory pluginSettingsFactory, BeerSupplyProjectCreator projectCreator, BeerSupplyWorkflowCreator workflowCreator, BeerSupplyScreenCreator screenCreator, BreweryDBClient breweryDBClient)
    {
        this.pluginSettingsFactory = checkNotNull(pluginSettingsFactory);
        this.projectCreator = projectCreator;
        this.workflowCreator = workflowCreator;
        this.screenCreator = screenCreator;
        this.breweryDBClient = breweryDBClient;
    } */
    
    private final PluginSettingsFactory pluginSettingsFactory;
    private final BreweryDBClient breweryDBClient;
    
    public BeerSupplyConfigurationManager(PluginSettingsFactory pluginSettingsFactory, BreweryDBClient breweryDBClient)
        {
            this.pluginSettingsFactory = checkNotNull(pluginSettingsFactory);
            this.breweryDBClient = breweryDBClient;
            setBreweryDBApiKey("b937d4a045cc3aeb1358a2d7196c68e6");
        }

    public boolean isProjectConfigured()
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();

        return StringUtils.isNotBlank((String) settings.get(SETTINGS_PROJECT_KEY));
    }

    public boolean hasApiKey()
    {
        return StringUtils.isNotBlank(getBreweryDBApiKey());
    }

    public String getBreweryDBApiKey()
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
        return (String)settings.get(SETTINGS_BREWERYDB_KEY);
    }

    public void setBreweryDBApiKey(String key)
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
        settings.put(SETTINGS_BREWERYDB_KEY,key);
    }

}
