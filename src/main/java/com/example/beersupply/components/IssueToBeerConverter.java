package com.example.beersupply.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.sal.api.message.I18nResolver;

import com.example.beersupply.model.Beer;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;


/**
 * @since version
 */
public class IssueToBeerConverter
{
    private final ProjectManager projectManager;
    private final IssueManager issueManager;
    private final CustomFieldManager customFieldManager;
    private final I18nResolver i18n;
    private final WebResourceUrlProvider webResourceUrlProvider;
    private final BeerRatingManager beerRatingManager;

    public IssueToBeerConverter(ProjectManager projectManager, IssueManager issueManager, CustomFieldManager customFieldManager, I18nResolver i18n, WebResourceUrlProvider webResourceUrlProvider, BeerRatingManager beerRatingManager)
    {
        this.projectManager = projectManager;
        this.issueManager = issueManager;
        this.customFieldManager = customFieldManager;
        this.i18n = i18n;
        this.webResourceUrlProvider = webResourceUrlProvider;
        this.beerRatingManager = beerRatingManager;
    }

    public List<Beer> getBeersFromIssues(User user) throws GenericEntityException
    {
        String projectKey = i18n.getText("beer-supply.project.key");
        Project project = projectManager.getProjectObjByKey(projectKey);

        Collection<Long> issueIds = issueManager.getIssueIdsForProject(project.getId());

        if(null == issueIds || issueIds.isEmpty())
        {
            return Collections.<Beer> emptyList();
        }

        List<Issue> issues = issueManager.getIssueObjects(issueIds);

        List<Beer> beers = new ArrayList<Beer>(issues.size());

        for(Issue issue : issues)
        {
            String key = issue.getKey();
            String name = issue.getSummary();
            String desc = issue.getDescription();

            String style = getStyle(issue);

            Double userRating = beerRatingManager.getUserRating(key,user);

            beers.add(new Beer(key,name,style,desc,"",0.0,userRating));
        }

        return beers;
    }

    private String getStyle(Issue issue)
    {
        String style = "";

        final CustomField styleField = customFieldManager.getCustomFieldObjectByName("Beer Style");

        String issueStyle = (String) styleField.getValue(issue);
        if(null != issueStyle)
        {
            style = issueStyle;
        }

        return style;
    }
}
