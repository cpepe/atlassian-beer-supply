package com.example.beersupply.brewerydb.representations;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugin.webresource.UrlMode;

import org.apache.commons.lang.StringUtils;

/**
 * @since version
 */
@XmlRootElement(name = "beer")
@XmlAccessorType(value = XmlAccessType.PUBLIC_MEMBER)
public class BeerDescriptor
{
    private String name;
    private String description;
    private Map<String,String> labels;
    private Style style;
    private String icon;
    private String label;
    private String styleName;
    private List<Brewery> breweries;

    public String getName()
    {
        return (null != name) ? name : "";
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        String desc = "";
        
        if(null != description)
        {
            desc = description;
        }
        else if(null != style)
        {
            desc = style.getDescription();
        }
        
        return desc;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getIcon()
    {
        if(null == icon)
        {
            if(null != labels)
            {
                icon = labels.get("icon");
            }
        }
        
        return icon;
    }
    
    public void setIcon(String icon)
    {
        this.icon = icon;
    }
    
    public String getLabel()
    {
        if(null == label)
        {
            if(null != labels)
            {
                label = labels.get("medium");
            }
        }
        
        return label;   
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getStyle()
    {
        if(null == styleName)
        {
            if(null != style)
            {
                styleName = style.getName();
            }
        }
        return styleName;
    }

    public void setStyle(String styleName)
    {
        this.styleName = styleName;
    }
    
    public String getBrewery()
    {
        if(null != breweries && !breweries.isEmpty())
        {
            return breweries.get(0).getName();
        }
        
        return "";
    }
    
    public void setBrewery(String brewery)
    {
        //do nothing
    }
}
